local M = {}

local utils = require('utils')
local path = require('pl.path')

function M.generate_music(sakyra)
	local music_buf = {}
	for filename, _ in utils.dirtree(sakyra.assets_root .. sakyra.music_root) do
		if utils.file_extension(filename) ~= ".ogg" then
			goto continue
		end

		music_buf[#music_buf + 1] = '\t"'
		music_buf[#music_buf + 1] =
		    string.sub(filename, sakyra.root_len + 1, string.len(filename))
		-- len - 4 to exclude extension
		sakyra.music_id_buf[#sakyra.music_id_buf + 1] = music_buf[#music_buf]
		    :sub(sakyra.music_len + 1, music_buf[#music_buf]:len() - 4)
		    :escape_c()
		    :lower()
		music_buf[#music_buf + 1] = '",\n'
		::continue::
	end

	if #sakyra.music_id_buf == 0 then
		return
	end

	print(string.format("[CODE] generating music to %s", sakyra.generated_code_folder .. "musics.h"))
	local music_h = io.open(sakyra.generated_code_folder .. "musics.h", "w")
	if music_h then
		music_h:write(string.format([[
#ifndef %s_MUSIC_DATA_H
#define %s_MUSIC_DATA_H

#include <sakyra/std/audio.h>

enum music_id {
MUSIC_%s,
MUSIC_COUNT
};

extern const char *music_sources[];
extern struct se_music musics[];

#ifdef INCLUDE_SOURCES
const char *music_sources[MUSIC_COUNT] = {
%s};
struct se_music musics[MUSIC_COUNT];
#endif // INCLUDE_SOURCES

#endif // %s_MUSIC_DATA_H
	]], sakyra.project_short_name_upper, sakyra.project_short_name_upper, table.concat(sakyra.music_id_buf, ',\nMUSIC_'),
			table.concat(music_buf),
			sakyra.project_short_name_upper))
	end
end

function M.generate_sounds(sakyra)
	local sound_buf = {}
	for filename, _ in utils.dirtree(sakyra.assets_root .. sakyra.sound_root) do
		if utils.file_extension(filename) ~= ".wav" then
			goto continue
		end

		sound_buf[#sound_buf + 1] = '\t"'
		sound_buf[#sound_buf + 1] =
		    string.sub(filename, sakyra.root_len + 1, string.len(filename))
		-- len - 4 to exclude extension
		sakyra.sound_id_buf[#sakyra.sound_id_buf + 1] = sound_buf[#sound_buf]
		    :sub(sakyra.sound_len + 1, sound_buf[#sound_buf]:len() - 4)
		    :escape_c()
		    :lower()
		sound_buf[#sound_buf + 1] = '",\n'
		::continue::
	end

	if #sakyra.sound_id_buf == 0 then
		return
	end

	print(string.format("[CODE] generating sounds to %s", sakyra.generated_code_folder .. "sounds.h"))
	local sounds_h = io.open(sakyra.generated_code_folder .. "sounds.h", "w")
	if sounds_h then
		sounds_h:write(string.format([[
#ifndef %s_SOUNDS_DATA_H
#define %s_SOUNDS_DATA_H

#include <sakyra/std/audio.h>

enum sound_id {
SOUND_%s,
SOUND_COUNT
};

extern const char *sound_sources[];
extern struct se_sound sounds[];

#ifdef INCLUDE_SOURCES
const char *sound_sources[SOUND_COUNT] = {
%s};
struct se_sound sounds[SOUND_COUNT];
#endif // INCLUDE_SOURCES

#endif // %s_SOUNDS_DATA_H
	]], sakyra.project_short_name_upper, sakyra.project_short_name_upper, table.concat(sakyra.sound_id_buf, ',\nSOUND_'),
			table.concat(sound_buf),
			sakyra.project_short_name_upper))
	end
end

return M
