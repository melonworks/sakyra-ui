local M = {}

local function generate_properties(sakyra, json)
	local props = io.open(sakyra.assets_root .. "/propertytypes.json", "r")
	if not props then
		return
	end

	local tiled, _, err = json.decode(props:read("*a"), 1, nil)
	if err then
		print("err:" .. err)
		return
	end

	for _, value in ipairs(tiled) do
		if value["type"] ~= "enum" then
			goto next
		end

		local name = value["name"]:upper()
		local flag_count
		if value["name"]:find("flag") then
			-- if it's a flag, we also generate the word count for that flag
			-- 32 is the default cmc_bitset_word size
			flag_count = "#define " .. name .. "_WORD_COUNT " .. (math.ceil(#value["values"] / 32))
		end
		local enum_h = io.open(string.format("%s/%s.h", sakyra.generated_code_folder, value["name"]), "w")
		if enum_h then
			local sep = ",\n" .. name .. "_"
			enum_h:write(string.format([[
#ifndef %s_DATA_H
#define %s_DATA_H

enum %s {
%s_%s,
%s_COUNT
};

%s
#endif // %s_DATA_H]], name, name, value["name"], name, table.concat(value["values"], sep), name, flag_count or "",
				name))
		end

		::next::
	end
end

local function each_layer(utils, layer, prefix, objs)
	for _, obj in ipairs(layer["objects"]) do
		if utils.is_empty(obj["type"]) then
			goto continue
		end

		if not utils.is_empty(obj["name"]) then
			local name = string.format("%s_%s", obj["name"]:gsub(" ", "_"),
				obj["id"])
			table.insert(objs,
				string.format("%s_%s = %s", prefix, name, obj["id"]))
		end
		::continue::
	end
end

local function generate_ids(sakyra, json)
	local utils = require('utils')
	local path = sakyra.assets_root .. sakyra.map_root
	local prefix = "MAP_OBJ"

	-- if map_root is not defined, we set depth of our .tmj search to 1,
	-- since we dont want to recursively search the entire project
	local depth = -1
	if utils.is_empty(sakyra.map_root) then
		depth = 1
	end

	local objs = {}
	for filename, _ in utils.dirtree(path, depth) do
		if utils.file_extension(filename) ~= ".tmj" then
			goto continue
		end

		local props = io.open(filename, "r")
		if not props then
			return
		end

		local tiled, _, err = json.decode(props:read("*a"), 1, nil)
		if err then
			print("err:" .. err)
			return
		end

		for _, layer in ipairs(tiled["layers"]) do
			if layer["class"] ~= "objects" or layer["objects"] == nil then
				goto continue_layer
			end
			each_layer(utils, layer, prefix, objs)
			::continue_layer::
		end
		::continue::
	end

	local enum_name = "map_objs"
	local enum_h = io.open(string.format("%s/%s.h", sakyra.generated_code_folder, enum_name), "w")
	if enum_h then
		local sep = ",\n"
		enum_h:write(string.format([[
#ifndef %s_DATA_H
#define %s_DATA_H

enum %s {
%s_%s,
%s_COUNT = %d
};

#endif // %s_DATA_H
]], prefix, prefix, enum_name, prefix, table.concat(objs, sep), prefix,
			#objs, prefix))
	end
end

function M.generate_tiled(sakyra)
	print("[CODE] generating tiled enums")
	local ok, json = pcall(require, "dkjson")
	if not ok then
		return
	end
	generate_properties(sakyra, json)
	generate_ids(sakyra, json)
end

-- experimental
local function fix_tiled_map(path)
	local ok_json, json = pcall(require, "json")
	if not ok_json then
		print("lua-json not installed")
		return
	end

	local file = io.open(path, "r")
	if not file then
		return
	end

	local data = file:read("*a")
	local tilemap = json.decode(data)
	local firstgid = 1
	local map = {}

	for _, v in pairs(tilemap["tilesets"]) do
		v["firstgid"] = firstgid
		local id = 0
		for _, tile in pairs(v["tiles"]) do
			print(tile["id"])
			if tile["id"] ~= id then
				map[tile["id"]] = id
			end
			tile["id"] = id
			id = id + 1
		end
		firstgid = firstgid + v["tilecount"] - 1
	end
	-- print(dump(map))

	for _, layer in pairs(tilemap["layers"]) do
		if not layer["chunks"] then
			goto next
		end

		for _, chunk in pairs(layer["chunks"]) do
			for i, id in ipairs(chunk["data"]) do
				for k, v in ipairs(map) do
					print(k .. " to " .. v)
					if k == id then
						chunk["data"][i] = v
					end
				end
			end
		end
		::next::
	end

	file:close()
	local filew = io.open(path, "w")
	if filew then
		filew:write(json.encode(tilemap))
	end
end

return M
