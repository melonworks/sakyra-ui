local M = {}

local ok, lfs = pcall(require, "lfs")

function string:escape_c()
	return self:gsub(".", { ["/"] = "_", ["-"] = "_" })
end

function string:split(sep)
	if sep == nil then
		sep = "%s"
	end
	local t = {}
	for str in self:gmatch("([^" .. sep .. "]+)") do
		table.insert(t, str)
	end
	return t
end

function M.tablelength(T)
	local count = 0
	for _ in pairs(T) do count = count + 1 end
	return count
end

function M.is_empty(s)
	return s == nil or s == ''
end

function M.dirtree(dir, depth)
	if depth == nil then
		depth = -1
	end

	if string.sub(dir, -1) == "/" then
		dir = string.sub(dir, 1, -2)
	end

	local curr_depth = 0
	local function yieldtree(dir)
		for entry in lfs.dir(dir) do
			if entry ~= "." and entry ~= ".." then
				entry = dir .. "/" .. entry
				local attr = lfs.attributes(entry)
				coroutine.yield(entry, attr)
				if (depth == -1 or curr_depth < depth) and attr.mode == "directory" then
					curr_depth = curr_depth + 1
					yieldtree(entry)
				end
			end
		end
	end

	return coroutine.wrap(function() yieldtree(dir) end)
end

function M.dump(o)
	if type(o) == 'table' then
		local s = '{ '
		for k, v in pairs(o) do
			if type(k) ~= 'number' then k = '"' .. k .. '"' end
			s = s .. '[' .. k .. '] = ' .. M.dump(v) .. ','
		end
		return s .. '} '
	else
		return tostring(o)
	end
end

function string:starts(starts)
	return self:sub(1, starts:len()) == starts
end

-- only returns last extension
function M.file_extension(path)
	return path:match("^.+(%..+)$")
end

function M.file_path(path)
	local start = path:find("/[^/]*$")
	return path:sub(0, start)
end

function M.file_name(path)
	local start = path:find("/[^/]*$")
	return path:sub(start + 1, path:len())
end

function M.file_name_no_ext(path)
	local start = path:find("/[^/]*$")
	local iend = path:find("%.[^%.]*$") or path:len()
	return path:sub(start + 1, iend)
end

return M
