local M = {}
local utils = require('utils')

-- we pre-multiply the alpha channel with "-channel rgb -fx "u*u.a""
-- we also force srgb colorspace with "-colorspace srgb -type TrueColorAlpha"
-- or we might lose color information during the multiplication
local convert_premultiply = [[magick "%s" -colorspace srgb -type TrueColorAlpha -channel rgb -fx "u*u.a" "%s" &]]
local convert = [[magick "%s" "%s" &]]

function M.convert_image_from_to(filename, from_ext, to_ext, quiet)
	if utils.file_extension(filename) ~= from_ext then
		return false
	end

	local out = filename:gsub(from_ext, to_ext)
	if not quiet then
		print("converting (pre-multiply) " .. filename .. " to " .. to_ext);
	end
	-- print("converting " .. filename .. " to " .. to_ext);
	-- io.popen(string.format(convert, filename, out)):close()
	io.popen(string.format(convert_premultiply, filename, out)):close()
	return true
end

function M.convert_images(sakyra, from, to, quiet)
	local count = 0
	local total = 0
	for filename, _ in utils.dirtree(sakyra.assets_root .. sakyra.textures_root) do
		if (M.convert_image_from_to(filename, from, to, quiet)) then
			count = count + 1
		end
		total = total + 1
	end

	if not quiet then
		print(string.format("converted %d of %d to .qoi", count, total))
	end
end

return M
