local M = {}

local path = require('pl.path')

local is_build2 = nil

function M.generate_compile_commands()
	if not M.is_build2() then
		print("not a build2 project")
		return
	end

	print("generating compile_commands.json")
	os.execute("b -vn clean update 2>&1 | compiledb &")
end

function M.is_build2()
	if is_build2 == nil then
		is_build2 = path.exists("manifest")
	end
	return is_build2
end

return M
