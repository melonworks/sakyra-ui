local M = {}
local utils = require('utils')

local function load_textures_from_map(tex_id_buf)
	local textures_map = io.open("textures.map", "r")
	if textures_map then
		for line in textures_map:lines() do
			table.insert(tex_id_buf, line)
		end
	end
end

-- format:
-- [s16 Version][s32 Animation Count]
-- Animation Count ...{
--	[s16 Frame Count]
--	Frame Count ...{
--		[s16 Texture Id]
--		[float Time]
--		[float Origin X]
--		[float Origin Y]
--	}
-- }
local function compile(sakyra, content)
	local tex_sub = {}
	for k, v in pairs(sakyra.tex_id_buf) do
		tex_sub[v] = k - 1
	end


	local bin = {}
	table.insert(bin, string.pack(">hi", 1, utils.tablelength(content["animations"])))
	for k in pairs(content["animations"]) do
		table.insert(bin,
			string.pack(">h", #content["animations"][k]["rects"]))
		for _, v in ipairs(content["animations"][k]["rects"]) do
			local tex_id = v["tex"]:gsub("%S+", tex_sub)
			tex_id = tonumber(tex_id)
			if tex_id == nil then
				print(string.format("texture '%s' not found, ignoring ...", v["tex"]))
				goto continue
			end
			table.insert(bin,
				string.pack(">hfff", tex_id, v["time"], v["origin"]["x"], v["origin"]["y"]))
			:: continue::
		end
	end
	return table.concat(bin)
end

local function parse(content, anim_tbl)
	local anim_index = 0
	local animation = { animations = {}, origin = { x = 0, y = 0 } }
	for _, value in ipairs(content) do
		if value:starts("rect") then
			local rect = value:split(" ")
			local rect_tbl = {
				tex = "",
				time = 0.1,
				origin = {
					x = animation.origin["x"],
					y = animation.origin["y"],
				}
			}
			for i = 1, #rect do
				if rect[i] == "rect" then
					rect_tbl.tex = rect[i + 1]
					i = i + 1
				elseif rect[i] == "origin" then
					rect_tbl.origin.x = rect[i + 1]
					rect_tbl.origin.y = rect[i + 2]
					i = i + 2
				elseif rect[i] == "time" then
					rect_tbl.time = rect[i + 1]
					i = i + 1
				end
			end

			table.insert(animation["animations"][anim_index]["rects"], rect_tbl)
		elseif value:starts("origin") then
			local origin = value:split(" ")
			if #origin > 3 then
				print("invalid origin")
			end
			animation.origin = { x = origin[2], y = origin[3] }
		elseif value:starts("anim") then
			anim_index = anim_index + 1

			-- extract anim name
			local anim_name = ""
			local i = 0
			for name in value:gmatch("%S+") do
				if i == 1 then
					anim_name = name
					anim_tbl[anim_index] = name
				end
				i = i + 1
			end

			animation["animations"][anim_index] = { name = anim_name, rects = {} }

			if i == 1 then
				anim_tbl[anim_index] = "ANIM_unnamed" .. anim_index
			end
		end
	end
	return animation
end

function M.generate(sakyra)
	if #sakyra.tex_id_buf == 0 then
		load_textures_from_map(sakyra.tex_id_buf)
	end

	local anim_tbl = {}
	for filename, _ in utils.dirtree(sakyra.assets_root .. sakyra.generated_data_root) do
		if utils.file_extension(filename) ~= ".in" then
			goto continue
		end

		local anim_out_name = filename:gsub(".in", "")
		print(string.format("[ASSET] generating anim to %s", anim_out_name))

		local anim_in = io.open(filename, "r")
		if not anim_in then
			goto continue
		end

		-- extract lines
		local content = {}
		for line in anim_in:lines() do
			table.insert(content, line)
		end
		anim_in:close()

		local animation = parse(content, anim_tbl)
		local bin = compile(sakyra, animation)

		local anim_out = io.open(anim_out_name, "w")
		if anim_out then
			anim_out:write(bin)
		end

		local anim_type_unique = {}
		for _, value in ipairs(anim_tbl) do
			local type = value:split("_")[1]
			anim_type_unique[type] = type
		end

		local anim_type_tbl = {}
		for key, _ in pairs(anim_type_unique) do
			table.insert(anim_type_tbl, key)
		end

		local anim_h = io.open(sakyra.generated_code_folder .. "/anims.h", "w")
		if anim_h then
			anim_h:write(string.format([[
#ifndef %s_ANIMS_H
#define %s_ANIMS_H

#include <sakyra/std/the_animator.h>

enum anims_types {
ANIM_TYPE_%s,
	ANIMATIONS_TYPE_COUNT
};

enum anims_id {
ANIM_%s,
	ANIMATIONS_COUNT
};

extern struct se_sprite_animations animations;

#ifdef INCLUDE_SOURCES
struct se_sprite_animations animations;
#endif // INCLUDE_SOURCES

#endif // %s_ANIMS_H]], sakyra.project_short_name_upper, sakyra.project_short_name_upper,
				table.concat(anim_type_tbl, ',\nANIM_TYPE_'),
				table.concat(anim_tbl, ',\nANIM_'),
				sakyra.project_short_name_upper))
		end
		::continue::
	end
end

return M
