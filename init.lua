#!/bin/lua5.4

local is_sakyra, sakyra = pcall(require, "sakyra")
if not is_sakyra then
	sakyra = {}
end
sakyra.project_short_name_upper = nil
sakyra.root_len = 0
sakyra.textures_len = 0
sakyra.sound_len = 0
sakyra.music_len = 0
sakyra.font_len = 0
sakyra.tex_id_buf = {}
sakyra.sound_id_buf = {}
sakyra.font_id_buf = {}
sakyra.music_id_buf = {}

require('pl.app').require_here(nil, false)
local build2 = require('build2')
require('utils') -- for string:starts()

-- TODO: only modified files
-- local time, err = lfs.attributes(arg[1], 'modification')

-- MAIN

local cli = {
	["help"] = { desc = "shows this help" },
	["gen"] = { desc = "tex/anim/tiled/texts/locale/audio/sound/music/font" },
	["check"] = { desc = "generates" },
	["conv"] = { desc = "generates" },
	["fix"] = { desc = "generates" },
	["new"] = { desc = "generates" },
}
cli["-h"] = cli["help"];

if #arg == 0 then
	-- if nothing passed, just generate compile_commands if build2
	return 0
end

if arg[1]:starts("gen") then
	if is_sakyra then
		sakyra.project_short_name_upper = string.upper(sakyra.project_short_name)
		    :gsub(".", { ["/"] = "_", ["-"] = "_" })
		sakyra.root_len = string.len(sakyra.assets_root)
		sakyra.textures_len = string.len(sakyra.textures_root)
		sakyra.sound_len = string.len(sakyra.sound_root)
		sakyra.music_len = string.len(sakyra.music_root)
		sakyra.font_len = string.len(sakyra.font_root)
	else
		print("not a sakyra project")
	end

	-- validate variables
	if sakyra.root_len == 0 then
		print('invalid assets_root')
		os.exit(1)
	end

	if is_sakyra then
		if arg[2] == "tex" then
			require('texture').generate_textures(sakyra)
			print("You might want to regenerate animations too\n$ sakyra gen anim");
		elseif arg[2] ~= nil and arg[2]:starts("anim") then
			require('animation').generate(sakyra)
		elseif arg[2] == "tiled" then
			require('tiled').generate_tiled(sakyra)
		elseif arg[2] ~= nil and arg[2]:starts("text") then
			require('locale').generate_text(sakyra)
		elseif arg[2] == "locales" or arg[2] == "locale" then
			require('locale').generate_locales(sakyra)
		elseif arg[2] == "audio" then
			local audio = require('audio')
			audio.generate_sounds(sakyra)
			audio.generate_music(sakyra)
		elseif arg[2] == "sound" then
			require('audio').generate_sounds(sakyra)
		elseif arg[2] == "music" then
			require('audio').generate_music(sakyra)
		elseif arg[2] == "font" then
			require('font').generate_fonts(sakyra)
		elseif arg[2] == "compiledb" then
			build2.generate_compile_commands()
		else
			require('texture').generate_textures(sakyra)
			local audio = require('audio')
			audio.generate_sounds(sakyra)
			audio.generate_music(sakyra)
			require('font').generate_fonts(sakyra)
			require('tiled').generate_tiled(sakyra)
			-- locale.generate_text(sakyra)
			require('animation').generate(sakyra)
		end
	end
elseif arg[1] == "new" then
	require('project').new_project()
elseif arg[1] == "help" or arg[1] == "-h" then
	print("SAKYRA CLI")
	print(" commands:")
	for k, v in pairs(cli) do
		print(string.format("  %s  %s", k, v["desc"]))
	end
elseif arg[1] == "check" then
	-- cppcheck -j$(nproc) --enable=style sakyra/*.c
elseif arg[1]:starts("conv") then
	local conv = require('convert')
	local quiet = false
	local argc = #arg

	for i = 2, #arg do
		if arg[i] == "--quiet" or arg[i] == "-q" then
			quiet = true
			argc = argc - 1
			break
		end
	end

	if argc == 1 then
		if sakyra.assets_root == nil then
			print("assets_root not set")
			os.exit(1)
		end
		-- convert all images
		conv.convert_images(sakyra, ".png", ".qoi", quiet)
	else
		-- convert the given images
		for i = 2, #arg do
			conv.convert_image_from_to(arg[i], ".png", ".qoi", quiet)
		end
	end
end
