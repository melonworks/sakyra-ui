local M = {}

local utils = require('utils')
local path = require('pl.path')

function M.generate_fonts(sakyra)
	local fonts_buf = {}
	for filename, _ in utils.dirtree(sakyra.assets_root .. sakyra.font_root) do
		if utils.file_extension(filename) == ".ttf" then
			-- len - 4 to exclude extension
			fonts_buf[#fonts_buf + 1] = '\t"'
			fonts_buf[#fonts_buf + 1] =
			    string.sub(filename, sakyra.root_len + 1, string.len(filename))
			sakyra.font_id_buf[#sakyra.font_id_buf + 1] =
			    fonts_buf[#fonts_buf]
			    :sub(sakyra.font_len + 1, fonts_buf[#fonts_buf]:len() - 4)
			    :escape_c()
			    :lower()
			fonts_buf[#fonts_buf + 1] = '",\n'
		end
	end

	if #sakyra.font_id_buf == 0 then
		return
	end

	print(string.format("[CODE] generating fonts to %s", sakyra.generated_code_folder .. "fonts.h"))
	local fonts_h = io.open(sakyra.generated_code_folder .. "fonts.h", "w")
	if fonts_h then
		fonts_h:write(string.format([[
#ifndef %s_DATA_FONTS_H
#define %s_DATA_FONTS_H

#include <sakyra/std/text.h>

enum font_id {
FONT_%s,
FONT_COUNT
};

extern const char *font_sources[];
extern struct se_font fonts[];

#ifdef INCLUDE_SOURCES
const char *font_sources[FONT_COUNT] = {
%s};
struct se_font fonts[FONT_COUNT];
#endif // INCLUDE_SOURCES

#endif // %s_DATA_FONTS_H
	]], sakyra.project_short_name_upper, sakyra.project_short_name_upper, table.concat(sakyra.font_id_buf, ',\nFONT_'),
			table.concat(fonts_buf),
			sakyra.project_short_name_upper))
	end

	local font_map = io.open("fonts.map", "w")
	if font_map then
		font_map:write(table.concat(sakyra.font_id_buf, '\n'))
	end
end

return M
