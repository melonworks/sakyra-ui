local M = {}

local utils = require('utils')
local path = require('pl.path')
local lfsok, lfs = pcall(require, "lfs")

local lpeg
local stext_grammar
local locale_generated = false

local function make_stext_grammar()
	local merge = function() end -- forward declaration

	local Cf    = lpeg.Cf
	local Cg    = lpeg.Cg
	local Ct    = lpeg.Ct
	local C     = lpeg.C
	local P     = lpeg.P
	local S     = lpeg.S

	-- Config parameters
	local function doset(dest, name, value)
		if dest[name] == nil then
			dest[name] = value
		elseif type(dest[name]) ~= 'table' then
			dest[name] = { dest[name], value }
		else
			if type(value) == 'table' then
				for i = 1, #value do
					table.insert(dest[name], value[i])
					value[i] = nil
				end
				merge(dest[name], value)
			else
				table.insert(dest[name], value)
			end
		end
		return dest
	end

	merge         = function(dest, src)
		for name, value in pairs(src) do
			doset(dest, name, value)
		end
	end

	local CRLF    = P "\r" ^ -1 * P "\n"
	local SP      = P " " + P "\t"
	local EQ      = SP ^ 0 * P "=" * SP ^ 0
	local COMMA   = SP ^ 0 * P "," * SP ^ 0

	local comment = SP ^ 0 * (P ";" + P "#") * (P(1) - CRLF) ^ 0
	local blank   = SP ^ 0 * comment ^ -1 * CRLF

	local name    = C((P(1) - S " \t=[]") ^ 1)
	    / function(c) return c:lower() end

	local item    = C((P(1) - S ",;#\r\n") ^ 1)
	local char    = P(1) - P '"'
	local quoted  = P '"' * C(char ^ 0) * P '"'
	local value   = quoted + item
	local values  = Ct(value * (COMMA * value) ^ 1) * comment ^ -1
	    + value * comment ^ -1

	local pair    = SP ^ 0 * Cg(name * EQ * values) * CRLF
	local nvpairs = Cf(Ct "" * (blank + pair) ^ 0, doset)

	local sname   = SP ^ 0 * P "[" * SP ^ 0 * name * SP ^ 0 * P "]" * comment ^ -1 * CRLF
	local section = Cg(sname * nvpairs)

	local ini     = Cf(Ct "" * (blank + section + pair) ^ 1, doset)
	return ini
end

local function try_generate_locale_from_en(dir, loc)
	local text_root_en = dir .. 'en'
	local text_root_loc = dir .. loc .. '/'
	for filename, _ in utils.dirtree(text_root_en) do
		if utils.file_extension(filename) ~= ".spo" then
			goto continue
		end

		local new_filename = text_root_loc .. path.basename(filename)
		-- not exists
		if not lfs.attributes(new_filename) then
			io.popen(string.format([[cp -v "%s" "%s" &]], filename,
				new_filename)):close()
		end
		::continue::
	end
end

function M.generate_locales(sakyra)
	if not path.isdir(sakyra.locales) then
		print("locales dir not found")
		return
	end

	local supported_locales = { 'br', 'en', 'jp' }
	for _, loc in pairs(supported_locales) do
		lfs.mkdir(sakyra.locales .. loc)
		if loc ~= 'en' then
			print(string.format("generating %s locale", loc))
			try_generate_locale_from_en(sakyra.locales, loc)
		end
	end
end

local function gen_locale_header_from_table(sakyra, stext)
	local indexed_table = {}
	for k, _ in pairs(stext) do
		table.insert(indexed_table, k)
	end
	table.sort(indexed_table, function(a, b) return a < b end)
	-- print(utils.dump(indexed_table))

	if #indexed_table == 0 then
		return
	end

	print(string.format("[CODE] generating locale to %s", sakyra.generated_code_folder .. "locale.h"))
	local fonts_h = io.open(sakyra.generated_code_folder .. "locale.h", "w")
	if fonts_h then
		fonts_h:write(string.format([[
#ifndef %s_DATA_LOCALE_H
#define %s_DATA_LOCALE_H

enum locale_id {
S_%s,
S_COUNT
};

#endif // %s_DATA_LOCALE_H
	]], sakyra.project_short_name_upper, sakyra.project_short_name_upper, table.concat(indexed_table, ',\nS_'),
			sakyra.project_short_name_upper))
	end
end

local function gen_smo(sakyra, loc)
	local REVISION = 0

	for filename, _ in utils.dirtree(sakyra.locales .. loc) do
		if utils.file_extension(filename) ~= ".spo" then
			goto continue
		end

		local new_file = utils.file_name_no_ext(filename) .. "smo"
		local smo_path = string.format("%s%s/%s_%s", sakyra.assets_root, sakyra.generated_data_root, loc,
			new_file)
		local data = io.open(filename, "r")
		if data then
			-- load stext grammar to table
			local stext = lpeg.match(stext_grammar, data:read("*a"))
			data:close()
			local smo = io.open(smo_path, "wb")

			-- generate locale header file only once
			if not locale_generated then
				locale_generated = true
				gen_locale_header_from_table(stext)
			end

			-- sort table alphabetically
			-- table.sort(stext, function(a,b) return indexed_table[] < b end)
			-- print(utils.dump(stext))
			if smo then
				local string_count = 0
				for _ in pairs(stext) do string_count = string_count + 1 end

				local string_table = {}
				smo:write(string.pack("<hi", REVISION, string_count))
				for _, v in pairs(stext) do
					string_table[#string_table + 1] = string.pack("s" .. v["text"]:len(), v["text"])
				end
				-- print(utils.dump(string_table))
				smo:write(table.concat(string_table))
				smo:close();
			end
		end
		print(string.format("[ASSET] generating text to %s", smo_path))
		::continue::
	end
end

function M.generate_text(sakyra)
	if not path.isdir(sakyra.locales) then
		print("locales dir not found")
		return
	end

	-- load lpeg and prepare the ambient
	if not lpeg then
		local ok, local_lpeg = pcall(require, "lpeg")
		if not ok then
			print("lpeg not installed, skipping locales")
			return
		end
		lpeg = local_lpeg
		lpeg.locale(lpeg) -- adds locale entries into 'lpeg' table
		stext_grammar = make_stext_grammar()
	end

	for loc in lfs.dir(sakyra.locales) do
		if loc ~= '.' and loc ~= '..' then
			gen_smo(sakyra, loc)
		end
	end
end

return M
