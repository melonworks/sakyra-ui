local M = {}

local utils = require('utils')
local path = require('pl.path')

function M.generate_textures(sakyra)
	local tex_buf = {}
	for filename, _ in utils.dirtree(sakyra.assets_root .. sakyra.textures_root) do
		if utils.file_extension(filename) == ".qoi" then
			-- len - 4 to exclude extension
			tex_buf[#tex_buf + 1] = '\t"'
			tex_buf[#tex_buf + 1] =
			    string.sub(filename, sakyra.root_len + 1, string.len(filename))
			sakyra.tex_id_buf[#sakyra.tex_id_buf + 1] =
			    tex_buf[#tex_buf]
			    :sub(sakyra.textures_len + 1, tex_buf[#tex_buf]:len() - 4)
			    :escape_c()
			    :lower()
			tex_buf[#tex_buf + 1] = '",\n'
		end
	end

	if #sakyra.tex_id_buf == 0 then
		return
	end

	print(string.format("[CODE] generating textures to %s", sakyra.generated_code_folder .. "textures.h"))
	local textures_h = io.open(sakyra.generated_code_folder .. "textures.h", "w")
	if textures_h then
		textures_h:write(string.format([[
#ifndef %s_TEXTURES_H
#define %s_TEXTURES_H

#include <sakyra/texture.h>

enum tex_id {
TEX_ID_%s,
TEXTURE_COUNT
};

extern const char *tex_sources[];
extern struct se_texture textures[];

#ifdef INCLUDE_SOURCES
const char *tex_sources[TEXTURE_COUNT] = {
%s};
struct se_texture textures[TEXTURE_COUNT];
#endif // INCLUDE_SOURCES

#endif // %s_TEXTURES_H
	]], sakyra.project_short_name_upper, sakyra.project_short_name_upper, table.concat(sakyra.tex_id_buf, ',\nTEX_ID_'),
			table.concat(tex_buf),
			sakyra.project_short_name_upper))
	end

	local textures_map = io.open("textures.map", "w")
	if textures_map then
		textures_map:write(table.concat(sakyra.tex_id_buf, '\n'))
	end
end

return M
