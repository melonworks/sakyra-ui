local M = {}

local function substitute_proj_name(path)
	print("patching " .. path)
	local file_read = io.open(path, "r")
	if file_read then
		local data = file_read:read("*a")
		data = data:gsub([[%%PROJ_NAME%%]], arg[2])
		file_read:close()
		local file_write = io.open(path, "w")
		if file_write then
			file_write:write(data):close()
		end
	end
end

function M.new_project()
	print("generating sakyra project" .. arg[2])
	local lfsok, lfs = pcall(require, "lfs")

	local proj_name = arg[2]

	-- create folder
	local status = os.execute(string.format([[mkdir -v %s]], proj_name))
	if not status then
		print("failed to create project...")
		return
	end

	-- extract into folder
	local extract = os.execute(string.format([[7z x ~/.local/share/sakyra/base-proj.7z -o%s/]], proj_name))
	if not extract then
		print("failed to extract base project...")
		return
	end
	print("extracted succesfuly")

	-- rename files and relevant contents to project name
	os.execute(string.format([[mv -v %s/proj %s/%s]], proj_name, proj_name, proj_name))
	os.execute(string.format([[mv -v %s/%s/proj %s/%s/%s]], proj_name, proj_name, proj_name, proj_name, proj_name))

	substitute_proj_name(string.format("%s/%s/manifest", proj_name, proj_name))
	substitute_proj_name(string.format("%s/%s/build/bootstrap.build", proj_name, proj_name))
	substitute_proj_name(string.format("%s/%s/%s/buildfile", proj_name, proj_name, proj_name))

	print(string.format("cd ./%s/%s", proj_name, proj_name))
	local suc, err = lfs.chdir(string.format("./%s/%s", proj_name, proj_name))
	if not suc then
		print(err)
	end

	-- create build configuration
	-- debug
	local gen_cmd = string.format([[bdep init -C ../%s-clang-debug @clang-debug cc config.c=clang \
config.cc.poptions="-D_DEBUG -DSE_DEBUG" \
config.cc.loptions="-rdynamic" \
config.cc.coptions="-g" config.cc.coptions="-g" \
--wipe]], proj_name)
	print(gen_cmd)
	os.execute(gen_cmd)
	-- release
	gen_cmd = string.format([[bdep init -C ../%s-gcc @gcc cc config.c=gcc --wipe]],
		proj_name);
	os.execute(gen_cmd)

	-- write sakyra project lua file
	local sakyra_lua = string.format([[
return {
	project_name = "%s",
	-- used for prefixes (header guards)
	project_short_name = "%s",
	locales = "locales/",
	-- where sakyra generated code files are stored
	generated_code_folder = "%s/data/",
	-- where all of your assets are stored, prefixes every _root below
	assets_root = "%s/assets/",
	-- where sakyra generated data files are stored
	generated_data_root = "data/",
	textures_root = "sprites/",
	sound_root = "sound/",
	font_root = "fonts/",
	music_root = "music/",
}
]], proj_name, proj_name, proj_name, proj_name)

	local sakyra_file = io.open("sakyra.lua", "w")
	if sakyra_file then
		sakyra_file:write(sakyra_lua):close()
		print("created sakyra.lua")
	end

	print("new project generated succesfuly!")
end

return M
